module JSON.Parser

import Language.JSON.Parser
import Language.JSON
import JSON.Record
import Data.Maybe
import Data.List
import Data.List.Quantifiers

record Universe where
  constructor MkU
  U : Type
  interpret : U -> Type

export
data JSONType = JSNull | JSBool | JSNum | JSString | JSArray (List JSONType) | JSObj (List (String, JSONType)) | JSOpt JSONType

mutual
  public export
  InterpIdris : JSONType -> Type
  InterpIdris JSNull = Unit
  InterpIdris JSBool = Bool
  InterpIdris JSNum = Double
  InterpIdris JSString = String
  InterpIdris (JSArray x) = HList (map InterpIdris x)
  InterpIdris (JSObj xs) = Record (toField xs)
  InterpIdris (JSOpt ty) = Maybe (InterpIdris ty)

  public export
  toField : List (String, JSONType) -> List FieldType
  toField [] = []
  toField ((x, y) :: xs) = x :! InterpIdris y :: toField xs


JSONUniverse : Universe
JSONUniverse = MkU
  JSONType
  InterpIdris

mutual
  public export
  InferType : JSON -> JSONType
  InferType JNull = JSNull
  InferType (JBoolean x) = JSBool
  InferType (JNumber x) = JSNum
  InferType (JString x) = JSString
  InferType (JArray xs) = JSArray (map InferType xs)
  InferType (JObject xs) = JSObj (args4Obj xs) -- JSObj (map (\x => (fst x, InferType (snd x))) xs)

  public export
  args4Obj : List (String, JSON) -> List (String, JSONType)
  args4Obj [] = []
  args4Obj ((x , y) :: xs) = (x, InferType y) :: args4Obj xs

public export
ParseJSON : (json : JSON) -> InterpIdris (InferType json)
ParseJSON JNull = MkUnit
ParseJSON (JBoolean x) = x
ParseJSON (JNumber x) = x
ParseJSON (JString x) = x
ParseJSON (JArray []) = []
ParseJSON (JArray (x :: xs)) = ParseJSON x :: ParseJSON (JArray xs)
ParseJSON (JObject []) = []
ParseJSON (JObject ((x, y) :: xs)) = x :-: ParseJSON y :: ParseJSON (JObject xs)

UserJSON : JSON
UserJSON = JObject [("user", JString "John"), ("age", JNumber 23)]

UserRecord : Record ["user" :! String, "age" :! Double]
UserRecord = ["user" :-: "John", "age" :-: 23]

checkSame : UserRecord === ParseJSON UserJSON
checkSame = Refl

export
parseFromString : (json : String) -> Maybe (parsed : JSON ** InterpIdris (InferType parsed))
parseFromString = map (\j => (j ** ParseJSON j)) . JSON.parse


{-
-}

