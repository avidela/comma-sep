module Data.CSV.Types

import Data.List.Quantifiers
import Data.Vect
import Data.DPair

-- a CSV document with a row header at the top and columns of the same type
-- also carries the number of rows as an index
public export
record CSVRowHeader (header : List (String, Type)) (rows : Nat) where
  constructor MkCSVRH
  columns : All (Vect rows . Builtin.snd) header

public export
record CSVColHeader (header : List Type) (cols : Nat) where
  constructor MkCSVCH
  rows : All (Vect cols) header

-- a parsable type with along with its parsing function
public export
Parsable : Type
Parsable = DPair Type (\x => String -> Maybe x)

public export
record Header where
  constructor MkHeader
  content : List (String, Type)

public export
record ParsableHeader where
  constructor MkPHeader
  content : List (String, Parsable)

export
(.asHeader) : ParsableHeader -> Header
(.asHeader) ph = MkHeader $ map (mapSnd (.fst)) ph.content
