module Data.CSV.Parser

import Data.CSV.Types
import Data.String.Parser
import Data.List
import Data.Vect
import Data.Singleton

public export
record RawCSV where
  constructor MkRawCSV
  content : List (List String)

%name RawCSV csv

export
(.getRow) : RawCSV -> Nat -> Maybe (List String)
(.getRow) csv n = getAt n csv.content

export
(.dropRow) : RawCSV -> Nat -> (List (List String))
(.dropRow) (MkRawCSV content) n = drop n content

export
(.getCol) : RawCSV -> Nat -> Maybe (List String)
(.getCol) csv n = getAt n (transpose csv.content)


data Choice : Vect n Type -> Type where
  Chosen : (i : Fin n) -> index i types -> Choice types

parameters (separator : Char)
-- hchainr : Monad m => ParseT m arg -> ParseT m (arg -> end -> end) -> ParseT m end -> ParseT m end
  psep : List Char -> (acc : List Char) -> Choice [(Singleton "EOI", String),
                                                   (Singleton "Continue", String, List Char),
                                                   (Singleton "Done", String, List Char),
                                                   (Singleton "Error", String)]
  psep [] acc = Chosen 0 (Val "EOI", pack (reverse acc))
  psep ('"' :: xs) acc
      = case span (/= '"') xs of
             (str, '"' :: ',' :: xs) => Chosen 2 (Val "Done", pack ('"' :: str ++ ['"']), xs)
             (str, '"' :: '\r' :: '\n' :: xs) => Chosen 2 (Val "Done", pack ('"' :: str ++ ['"']), xs)
             (str, '"' :: '\n' :: xs) => Chosen 2 (Val "Done", pack ('"' :: str ++ ['"']), xs)
             (str, rest) => Chosen 3 (Val "Error", "unexpected input: \{pack rest}")

  psep ('\r' :: '\n' :: xs) acc
      -- If it's a line return, skip
      = Chosen 2 (Val "Done", pack (reverse acc), xs)
  psep ('\n' :: xs) acc
      -- If it's a line return, skip
      = Chosen 2 (Val "Done", pack (reverse acc), xs)
  psep (x :: xs) acc = if x == separator
                          -- if it's the separator, return "continue" and the accumulator
                          then Chosen 1 (Val "Continue", pack (reverse acc), xs)
                          -- If it's not '\r\n', '\n' or the separator, keep going
                          else psep xs (x :: acc)

  pline : List Char -> (acc : List String) -> Either String (List String, List Char)
  pline [] acc = Right (reverse acc, [])
  pline xs acc = case psep xs [] of
                      (Chosen 0 (_, str)) => Right (reverse (str :: acc), [])
                      (Chosen 1 (_, str, Prelude.Nil)) => Right (reverse ("" :: str :: acc), [])
                      (Chosen 1 (_, str, rest)) => pline rest (str :: acc)
                      (Chosen 2 (_, str, rest)) => Right (reverse (str :: acc), rest)
                      (Chosen 3 (_, str)) => Left str

  pall : List Char -> (acc : List (List String)) -> Either String (List (List String))
  pall xs acc = case pline xs [] of
                     Left err => Left err
                     Right (parsed, []) => Right (reverse (parsed :: acc))
                     Right (parsed, rest) => pall rest (parsed :: acc)


-- Turn the string to csv

export
parseCSV : {default ',' separator : Char} -> (input : String) -> Either String RawCSV
parseCSV str = MkRawCSV <$> pall separator (unpack str) []

