module Data.CSV

import Data.List.Quantifiers
-- import Data.Vect.Quantifiers
import Data.Vect
import public Data.DPair
import Data.CSV.Types
import Data.CSV.Parser

data MPS = MkMPS Double
data Len = MkLen Double
data Mass = MkMass Double

headerType : List (String, Type)
headerType = [("speed", MPS), ("length",Len), ("mass", Mass)]

0 buildHeader : List (String, Parsable) -> List (String, Type)
buildHeader [] = []
buildHeader (x :: xs) = ((fst x, fst (snd x)) :: buildHeader xs)

export
interpretCSV : (header : List (String, Parsable)) ->
               (content : List (Vect m String)) ->
               Maybe (CSVRowHeader (buildHeader header) m)
interpretCSV [] content = Just $ MkCSVRH []
interpretCSV ((x, (ty ** p)) :: xs) (c :: cs) =
  MkCSVRH <$> (pure (::) <*> traverse p c <*> (columns <$> interpretCSV xs cs))
interpretCSV _ _ = Nothing

%unbound_implicits off
export
interpretCSVList : {0 m : Nat} -> (header : List (String, Parsable)) ->
               (content : List (List String)) ->
               Maybe (CSVRowHeader (map (mapSnd (.fst)) header) m)
interpretCSVList = ?rest
%unbound_implicits on


isIn : String -> List (String, a) -> Bool
isIn str [] = False
isIn str (x :: xs) = if fst x == str then True else isIn str xs

get : (h : String) -> (xs : List (String, a)) -> {auto p : (h `isIn` xs) === True} -> a
get h [] {p} = absurd p
get h (x :: xs) with (fst x == h)
  get h (x :: xs) | False = get h xs
  get h (x :: xs) | True = snd x

getCol : (headerName : String) -> {ht : List (String, Type)} -> {auto p : (headerName `isIn` ht) === True} ->
         CSVRowHeader ht m -> Vect m (get headerName ht)
getCol headerName x {ht = []} = absurd p
getCol headerName x {ht = (y :: xs)} with (fst y == headerName)
  getCol headerName (MkCSVRH (c :: cs)) {ht = (y :: xs)} | False = getCol headerName (MkCSVRH cs)
  getCol headerName (MkCSVRH (c :: cs)) {ht = (y :: xs)} | True = c

uniformMatrix : List (List a) -> Maybe (y ** List (Vect y a))
uniformMatrix [] = Just (Z ** [])
uniformMatrix (x :: xs) = map (MkDPair (length x)) (traverse (toVect (length x)) (x :: xs))

-- %unbound_implicits off
-- formatCSV : RawCSV -> (h : ParsableHeader)
--     -> Maybe (Exists (\n => CSVRowHeader (buildHeader h.asHeader) n))
-- formatCSV (MkRawCSV []) h = Nothing
-- formatCSV (MkRawCSV (firstRow :: rest)) h = do
--   let headerStrings = map fst h.content
--   when (firstRow == headerStrings) Nothing
--   (n ** matrixContent) <- uniformMatrix rest
--   interpreted <- interpretCSV (h.content) matrixContent
--   Just $ Evidence n (?formatCSV_rhs_0)
--
-- interpretCSV : (header : Vect n String) ->
--                (content : Vect n (Vect m String)) ->
--                (ht : Vect n (String, Type)) ->
--                Maybe (CSVRowHeader (map snd ht) m)
