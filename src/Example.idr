module Example

import Data.CSV
import Data.Singleton
import Data.String

HeaderFormat : Type
HeaderFormat = List (String, Parsable)

interface Parser ty where
  parse : String -> Maybe ty

MkFormat : String -> (x : Type) -> {auto s : Parser x} -> (String, Parsable)
MkFormat str x = (str, Evidence x parse)

data Mol = MkMol Double
data Meter = MkMeter Double
data Kg = MkKg Double

Parser Kg where
  parse x = MkKg <$> parseDouble x
Parser Mol where
  parse x = MkMol <$> parseDouble x
Parser Meter where
  parse x = MkMeter <$> parseDouble x

myDataFormat : HeaderFormat
myDataFormat = [ MkFormat "qty" Mol
               , MkFormat "height" Meter
               , MkFormat "mass" Kg
               ]

main : IO ()
main = do
  rawCSV <- parseCSVFile "data.csv"
  let Just csv = interpretCSVList {header = myDataFormat, content = rawCSV}
  putStrLn "parsing CSV"
