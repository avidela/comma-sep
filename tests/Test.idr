module Test

import Data.CSV.Parser
import System.File
import System
import Data.List

csvContent : List (List String)
csvContent =
    [ ["Industry"]
    , ["Accounting/Finance"]
    , ["Advertising/Public Relations"]
    , ["Aerospace/Aviation"]
    , ["Arts/Entertainment/Publishing"]
    , ["Automotive"]
    , ["Banking/Mortgage"]
    , ["Business Development"]
    , ["Business Opportunity"]
    , ["Clerical/Administrative"]
    , ["Construction/Facilities"]
    , ["Consumer Goods"]
    , ["Customer Service"]
    , ["Education/Training"]
    , ["Energy/Utilities"]
    , ["Engineering"]
    , ["Government/Military"]
    , ["Green"]
    , ["Healthcare"]
    , ["Hospitality/Travel"]
    , ["Human Resources"]
    , ["Installation/Maintenance"]
    , ["Insurance"]
    , ["Internet"]
    , ["Job Search Aids"]
    , ["Law Enforcement/Security"]
    , ["Legal"]
    , ["Management/Executive"]
    , ["Manufacturing/Operations"]
    , ["Marketing"]
    , ["Non-Profit/Volunteer"]
    , ["Pharmaceutical/Biotech"]
    , ["Professional Services"]
    , ["QA/Quality Control"]
    , ["Real Estate"]
    , ["Restaurant/Food Service"]
    , ["Retail"]
    , ["Sales"]
    , ["Science/Research"]
    , ["Skilled Labor"]
    , ["Technology"]
    , ["Telecommunications"]
    , ["Transportation/Logistics"]
    , ["Other"]]
runTest : Show a => Eq a => (description : String) -> (actual, expected : a) -> IO ()
runTest desc actual expected = unless (actual == expected) $ do
  putStrLn ("Test failed: " ++ desc)
  putStrLn ("Expected : " ++ show expected)
  putStrLn ("Got instead : " ++ show actual)
  exitFailure


main : IO ()
main = do
  Right file1 <- readFile "industry.csv"
  | Left err => printLn err *> exitFailure
  Right file2 <- readFile "industry_sic.csv"
  | Left err => printLn err *> exitFailure
  let Right csv1 = parseCSV file1
  | Left err => putStrLn "fail parsing file 1: \{show err}" *> exitFailure
  let Right csv2 = parseCSV file2
  | Left err => putStrLn "fail parsing file 2: \{show err}" *> exitFailure
  runTest "csv1" {actual = length csv1.content, expected = 44}
  runTest "csv1Content" {actual = csv1.content, expected = csvContent}
  runTest "csv2" {actual = length csv2.content, expected = 732}
  runTest "csv2Content" {actual = tail' csv2.content >>= head', expected = Just ["1110",#""Growing of cereals (except rice), leguminous crops and oil seeds""#]}
  putStrLn "test successful"
